﻿using Microsoft.AspNetCore.SignalR.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LINQ
{
    class HubService
    {
        HubConnection _connection;
        public HubService(string url)
        {
            _connection = new HubConnectionBuilder()
                .WithUrl(url)
                .Build();
        }

        public HubConnection Connection => _connection;

        public async void StartConnection()
        {
            _connection.Closed += async (error) =>
            {
                await Task.Delay(new Random().Next(0, 5) * 1000);
                await _connection.StartAsync().ContinueWith(task => ConnectionResult(task));
            };

            _connection.On<string>("displayMessage", (message) =>
            {
                Console.WriteLine($"\tServer corresponds with: {message}");
            });

            await _connection.StartAsync().ContinueWith(task => ConnectionResult(task)); ;
        }

        private void ConnectionResult(Task task)
        {
            if (task.IsFaulted)
            {
                Console.WriteLine("There was an error opening the connection");
            }
            else
            {
                Console.WriteLine("Connected to server");
            }
        }
    }
}
