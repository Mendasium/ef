﻿using LINQ.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LINQ
{
    class ProjectInterface
    {
        string[] lINQmenu;
        string[] menu;
        string[] CRUD;
        string[] dataTypes;
        ProjectsService projectsService;
        public ProjectInterface(string APIUrl)
        {
            projectsService = new ProjectsService(APIUrl);
            menu = new string[]
            {
                "Work with projects",
                "Work with states",
                "Work with Tasks",
                "Work with Teams",
                "Work with Users",
                "Show Data",
                "Show all messages"
            };
            CRUD = new string[]
            {
                "Create",
                "Read",
                "Update",
                "Delete"
            };
            lINQmenu = new string[]
            {
                "Show projects and its tasksCount by UserId",
                "Show tasks with name shorter then 45 symbols by UserId",
                "Show (id,name) tasks, that was finished this year by UserId",
                "Show list of teams, all members of which are older than 12 years, sorted by registrationDate and grouped by teams",
                "Show list of users, sorted alphabetically with sorted tasks by name length",
                "Show structure, connected with user and his project, by its id",
                "Show structure, connected with Project and its tasks, by project id"
            };
            dataTypes = new string[]
            {
                "project",
                "state",
                "task",
                "team",
                "user"
            };
        }

        public void Start()
        {
            TypeInfo("To hide current menu type \"0\"");
            int result = 0;
            while (true)
            {
                result = GetMenuItem(menu);
                if (result > 0 && result < 6)
                {
                    CRUDElement(result);
                    continue;
                }   
                switch (result)
                {
                    case 6:
                        ShowLINQMenu();
                        break;
                    case 7:
                        ShowMessages();
                        break;
                    case 0:
                        return;
                    default:
                        TypeInfo("Enter correct menu option");
                        break;
                }
            }
        }
        private void CRUDElement(int elementId)
        {
            int result = 0;
            string element = dataTypes[elementId - 1];
            while (true)
            {
                result = GetMenuItem(CRUD.Select(x => x + " " + element).ToArray());
                switch (result)
                {
                    case 1:
                        AddAll(elementId);
                        break;
                    case 2:
                        ShowAll(elementId);
                        break;
                    case 3:
                        UpdateAll(elementId);
                        break;
                    case 4:
                        Delete(elementId, GetId());
                        break;
                    case 0:
                        return;
                    default:
                        TypeInfo("Enter correct menu option");
                        break;
                }
            }
        }
        
        private void ShowLINQMenu()
        {
            int result = 0;
            while (true)
            {
                result = GetMenuItem(lINQmenu);
                switch (result)
                {
                    case 1:
                        ShowProjectTasksCountByUserId();
                        break;
                    case 2:
                        ShowShortnameTasksByPerformerIdByUserId();
                        break;
                    case 3:
                        ShowFinishedTasksByUserId();
                        break;
                    case 4:
                        ShowSortedTeams();
                        break;
                    case 5:
                        ShowSortedUsersWithTasks();
                        break;
                    case 6:
                        ShowUserInfoById();
                        break;
                    case 7:
                        ShowProjectInfoById();
                        break;
                    case 0:
                        return;
                    default:
                        TypeInfo("Enter correct menu option");
                        break;
                }
            }
        }

        private int GetId()
        {
            TypeInfo("Enter Id");
            int res = -1;
            while (res == -1)
            {
                string result = Console.ReadLine();
                if (!int.TryParse(result, out res) || res <= 0)
                {
                    TypeInfo("Enter correct id, please");
                    res = -1;
                    continue;
                }
            }
            return res;
        }

        private int GetMenuItem(string[] menu)
        {
            for (int i = 1; i <= menu.Length; i++)
            {
                TypeInfo(i + " - " + menu[i - 1]);
            }
            Console.WriteLine();
            string result = "";
            int res = -1;
            while (res < 0)
            {
                result = Console.ReadLine();
                if (!int.TryParse(result, out res) || (res > 7 || res <= 0))
                {
                    res = -1;
                    TypeInfo("Please enter correct number");
                }
            }
            return res;
        }

        private void TypeInfo(string info)
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine(info);
            Console.ResetColor();
        }

        private void Delete(int elementId, int Id)
        {
            projectsService.Remove(dataTypes[elementId - 1], Id);
        }

        private void ShowMessages()
        {
            var result = projectsService.GetMessages();
            foreach(var str in result)
            {
                Console.WriteLine(str);
            }
        }

        #region ShowData
        private void ShowAll(int elementId)
        {
            Console.WriteLine($"All {dataTypes[elementId - 1]}s");
            switch (elementId)
            {
                case 1:
                    ShowList(projectsService.GetProjects());
                    break;
                case 2:
                    ShowList(projectsService.GetStates());
                    break;
                case 3:
                    ShowList(projectsService.GetTasks());
                    break;
                case 4:
                    ShowList(projectsService.GetTeams());
                    break;
                case 5:
                    ShowList(projectsService.GetUsers());
                    break;
            }
        }
        public void ShowList(dynamic list)
        {
            foreach(var item in list)
            {
                Console.WriteLine($"\t{item.ToString()}");
            }
        }
        #endregion

        #region AddUpdateData
        public void UpdateAll(int elementId)
        {
            Console.WriteLine($"Updating {dataTypes[elementId - 1]}");
            switch (elementId)
            {
                case 1:
                    projectsService.UpdateData(dataTypes[elementId - 1], AddUpdateProject(false));
                    break;
                case 2:
                    projectsService.UpdateData(dataTypes[elementId - 1], AddUpdateState(false));
                    break;
                case 3:
                    projectsService.UpdateData(dataTypes[elementId - 1], AddUpdateTask(false));
                    break;
                case 4:
                    projectsService.UpdateData(dataTypes[elementId - 1], AddUpdateTeam(false));
                    break;
                case 5:
                    projectsService.UpdateData(dataTypes[elementId - 1], AddUpdateUser(false));
                    break;
            }
        }
        public void AddAll(int elementId)
        {
            Console.WriteLine($"Adding {dataTypes[elementId - 1]}");
            switch (elementId)
            {
                case 1:
                    projectsService.AddData(dataTypes[elementId -1], AddUpdateProject());
                    break;
                case 2:
                    projectsService.AddData(dataTypes[elementId - 1], AddUpdateState());
                    break;
                case 3:
                    projectsService.AddData(dataTypes[elementId - 1], AddUpdateTask());
                    break;
                case 4:
                    projectsService.AddData(dataTypes[elementId - 1], AddUpdateTeam());
                    break;
                case 5:
                    projectsService.AddData(dataTypes[elementId - 1], AddUpdateUser());
                    break;
            }
        }
        public Project AddUpdateProject(bool add = true)
        {
            var p = new Project();
            if(!add)
                p.Id = GetInt("current project id");
            p.Name = GetString("project name");
            p.Description = GetString("project description");
            if (add)
            {
                p.AuthorId = GetInt("author Id");
                p.Deadline = GetDate("deadline date");
                p.TeamId = GetInt("team id");
            }
            return p;
                
        }
        public State AddUpdateState(bool add = true)
        {
            var s = new State();
            if (!add)
                s.Id = GetInt("current state id");
            s.Value = GetString("state value");
            return s;
        }
        public Task AddUpdateTask(bool add = true)
        {
            var t = new Task();
            if (!add)
                t.Id = GetInt("current task id");
            t.Name = GetString("task name");
            t.Description = GetString("task description");
            t.StateId = GetInt("state id");
            if (!add)
                t.FinishedAt = GetDate("finished date");
            if (add)
            {
                t.PerformerId = GetInt("performer Id");
                t.ProjectId = GetInt("project id");
            }
            return t;
        }
        public Team AddUpdateTeam(bool add = true)
        {
            var t = new Team();
            if (!add)
                t.Id = GetInt("current team id");
            t.Name = GetString("Team name");
            return t;
        }
        public User AddUpdateUser(bool add = true)
        {
            var u = new User();
            if (!add)
                u.Id = GetInt("current user id");
            u.TeamId = GetInt("team id", true);
            u.Email = GetString("email");
            if (add)
            {
                u.FirstName = GetString("first name");
                u.LastName = GetString("last name");
                u.Birthday = GetDate("birthday");
            }
            return u;

        }
        #endregion

        public int GetInt(string name, bool canBeNull = false)
        {
            string canNull = canBeNull ? "or 0 for null" : "";
            TypeInfo($"Enter {name} {canNull}");
            int res = -1;
            while (res == -1)
            {
                string result = Console.ReadLine();
                if (!int.TryParse(result, out res) || res <= 0)
                {
                    TypeInfo($"Enter correct {name}, please");
                    res = -1;
                    continue;
                }
                else if (canBeNull && res == 0)
                    return 0;
            }
            return res;
        }
        public string GetString(string name)
        {
            TypeInfo($"Enter {name}");

            string result = Console.ReadLine();
            return result;
        }
        public DateTime GetDate(string name)
        {
            TypeInfo($"Enter {name} in format \"{DateTime.Now.Date}\"");
            while (true)
            {
                string result = Console.ReadLine();
                if (!DateTime.TryParse(result, out DateTime res) || res.Year <= 1950)
                {
                    TypeInfo($"Enter correct {name}, please");
                    continue;
                }
                else
                    return res;
            }
        }

        #region ProjectTasksCountByUserId
        private void ShowProjectTasksCountByUserId()
        {
            TypeInfo("Enter UserId");
            var res = projectsService.GetProjectTasksCountByUserId(GetId());
            if (res.Count() == 0)
            {
                TypeInfo("Nothing Found");
                return;
            }
            TypeInfo("Result");
            foreach (var pair in res)
            {
                TypeInfo("Project:");
                Console.WriteLine("\t" + pair.Project);
                Console.WriteLine("Tasks count: " + pair.TaskCount);
            }
        }

        ////Получить кол-во тасков у проекта конкретного пользователя(по id) (словарь, где ключом будет проект, а значением кол-во тасков).
        //public Dictionary<Project, int> GetProjectTasksCountByUserId(int id)
        //{
        //    var tasks = projectsService.GetTasksAsync().Result;
        //    var projects = projectsService.GetProjectsAsync().Result;

        //    return tasks.Where(t => t.PerformerId == id)
        //        .Join(projects, x => x.ProjectId, y => y.Id, (x, y) => new Task(x) { Project = new Project(y) })
        //        .GroupBy(x => x.Project)
        //        .ToDictionary(x => x.Key, x => x.Count());
        //}
        #endregion

        #region ShortnameTasksByPerformerIdByUserId
        private void ShowShortnameTasksByPerformerIdByUserId()
        {
            TypeInfo("Enter UserId");
            var res = projectsService.GetShortnameTasksByPerformerIdByUserId(GetId());
            if (res.Count() == 0)
            {
                TypeInfo("Nothing Found");
                return;
            }
            TypeInfo("Result:");
            foreach (var el in res)
            {
                Console.WriteLine("\t" + el);
            }
        }

        ////Получить список тасков, назначенных на конкретного пользователя(по id), где name таска < 45 символов (коллекция из тасков).
        //public IEnumerable<Task> GetShortnameTasksByPerformerIdByUserId(int id)
        //{
        //    var projects = projectsService.GetTasksAsync().Result;
        //    var users = projectsService.GetUsersAsync().Result;

        //    return projects.Where(t => t.Name.Length < 45 && t.PerformerId == id)
        //        .Select(x =>
        //        {
        //            return new Task(x)
        //            {
        //                Performer = new User(users.FirstOrDefault(y => x.PerformerId == y.Id)),
        //                State = states.FirstOrDefault(s => s.Id == x.StateId)
        //            };
        //        });
        //}
        #endregion

        #region FinishedTasksByUserId
        private void ShowFinishedTasksByUserId()
        {
            TypeInfo("Enter UserId");
            var res = projectsService.GetFinishedTasksByUserId(GetId());
            if (res.Count() == 0)
            {
                TypeInfo("Nothing Found");
                return;
            }
            TypeInfo("Result:");
            foreach (var (Id, Name) in res)
            {
                Console.WriteLine("\tProject" + Id + " - " + Name);
            }
        }

        ////Получить список (id, name) из коллекции тасков, которые выполнены (finished) в текущем (2019) году для конкретного пользователя (по id).
        //public IEnumerable<(int Id, string Name)> GetFinishedTasksByUserId(int id, int year = 2019)
        //{
        //    var projects = projectsService.GetTasksAsync().Result;

        //    return projects.Where(t => t.PerformerId == id && t.FinishedAt.Year == year)
        //        .Select(x =>
        //        {
        //            return new Task(x)
        //            {
        //                State = states.FirstOrDefault(s => s.Id == x.StateId)
        //            };
        //        }).Where(t => t.State.Value.Equals("finished"))
        //        .Select(x => (Id: x.Id, Name: x.Name));
        //}
        #endregion

        #region SortedTeams
        private void ShowSortedTeams()
        {
            var res = projectsService.GetSortedTeams();
            if (res.Count() == 0)
            {
                TypeInfo("Nothing Found");
                return;
            }
            TypeInfo("Result:");
            foreach (var (id, name, userList) in res)
            {
                Console.WriteLine("\tProject:" + id + " - " + name);
                TypeInfo("\tUsers:");
                foreach (var u in userList)
                    Console.WriteLine("\t\t" + u.ToString());
            }
        }

        ////Получить список (id, имя команды и список пользователей) из коллекции команд, участники которых старше 12 лет, отсортированных по дате регистрации пользователя по убыванию, а также сгруппированных по командам.
        //public IEnumerable<(int id, string name, List<User> userList)> GetSortedTeams()
        //{//Команд в которых все учасники старше 12 лет нету
        //    var users = projectsService.GetUsersAsync().Result;
        //    var teams = projectsService.GetTeamsAsync().Result;

        //    return users.Join(teams, x => x.TeamId, y => y.Id, (x, y) => new User(x) { Team = y })
        //        .GroupBy(u => u.Team.Id)
        //        .Where(u => u.All(x => DateTime.Now.Year - x.Birthday.Year >= 12))
        //        .Select(t =>
        //            (id: t.Key, 
        //            name: teams.FirstOrDefault(x => x.Id == t.Key).Name, 
        //            userList: t.OrderByDescending(x => x.RegisteredAt)
        //                        .ToList())
        //            );
        //}
        #endregion

        #region SortedUsersWithTasks
        private void ShowSortedUsersWithTasks()
        {
            var res = projectsService.GetSortedUsersWithTasks();
            if (res.Count() == 0)
            {
                TypeInfo("Nothing Found");
                return;
            }
            TypeInfo("Result:");
            foreach (var (user, tasks) in res)
            {
                TypeInfo("\tUser:");
                Console.WriteLine("\t\t" + user);
                TypeInfo("\tTasks:");
                foreach (var t in tasks)
                    Console.WriteLine("\t\t\t" + t.ToString());
            }
        }

        ////Получить список пользователей по алфавиту first_name (по возрастанию) с отсортированными tasks по длине name (по убыванию).
        //public IEnumerable<(User user, List<Task> tasks)> GetSortedUsersWithTasks()
        //{
        //    var tasks = projectsService.GetTasksAsync().Result;
        //    var users = projectsService.GetUsersAsync().Result;

        //    return tasks.Join(users, x => x.PerformerId, y => y.Id, (x, y) => new Task(x) { Performer = new User(y) })
        //        .GroupBy(x => x.Performer.Id)
        //        .Select(x => (Performer: x.FirstOrDefault().Performer, Tasks: x.OrderByDescending(y => y.Name.Length).ToList()))
        //        .OrderBy(x => x.Performer.FirstName);
        //}
        #endregion

        #region UserInfoById
        private void ShowUserInfoById()
        {
            TypeInfo("Enter UserId");
            var res = projectsService.GetUserInfoById(GetId());
            if (res.User == null)
            {
                TypeInfo("Nothing Found");
                return;
            }
            TypeInfo("Result:");
            Console.WriteLine("\tUser: " + res.User);
            Console.WriteLine("\tLastProject: " + res.LastProject);
            Console.WriteLine("\tCanceledTasksCount: " + res.CanceledTasksCount);
            Console.WriteLine("\tLongestTask: " + res.LongestTask);
            Console.WriteLine("\tLastProjectUserTasks: " + res.LastProjectUserTasks);
        }

        ////Получить следующую структуру(передать Id пользователя в параметры) :
        ////User
        ////Последний проект пользователя(по дате создания)
        ////Общее кол-во тасков под последним проектом
        ////Общее кол-во незавершенных или отмененных тасков для пользователя
        ////Самый долгий таск пользователя по дате(раньше всего создан - позже всего закончен)
        ////P.S. - в данном случае, статус таска не имеет значения, фильтруем только по дате.
        //public (User User, Project LastProject, int CanceledTasksCount, RawTask LongestTask, int LastProjectUserTasks) GetUserInfoById(int id)
        //{
        //    var user = projectsService.GetUserByIdAsync(id).Result;
        //    var lastProject = new Project(projectsService.GetProjectsAsync().Result.OrderBy(x => x.CreatedAt).LastOrDefault());
        //    var tasks = projectsService.GetTasksAsync().Result.Where(t => t.PerformerId == id);

        //    return
        //        (
        //            User: new User(user),
        //            LastProject: lastProject,
        //            CanceledTasksCount: tasks.Join(states, x => x.StateId, y => y.Id, (x,y) => new Task(x) { State = y })
        //                .Count(x => x.State.Value == "Canceled" || x.State.Value == "Started"),
        //            LongestTask: tasks.OrderBy(x => x.FinishedAt - x.CreatedAt).FirstOrDefault(),
        //            LastProjectUserTasks: tasks.Count(x => x.ProjectId == lastProject.Id)
        //        );
        //}
        #endregion

        #region ProjectInfoById
        private void ShowProjectInfoById()
        {
            TypeInfo("Enter ProjectId");
            var res = projectsService.GetProjectInfoById(GetId());
            if (res.Project == null)
            {
                TypeInfo("Nothing Found");
                return;
            }
            TypeInfo("Result:");
            Console.WriteLine("\tProject: " + res.Project.ToString());
            Console.WriteLine("\tLongestTask: " + res.LongestTask.ToString());
            Console.WriteLine("\tShortestTask: " + res.ShortestTask.ToString());
            if (res.UsersCount != -1)
                Console.WriteLine("\tUsersCount: " + res.UsersCount);
        }
        ////Получить следующую структуру(передать Id проекта в параметры) :
        ////Проект
        ////Самый длинный таск проекта(по описанию)
        ////Самый короткий таск проекта(по имени)
        ////Общее кол-во пользователей в команде проекта, где или описание проекта > 25 символов или кол-во тасков < 3
        //public (Project Project, RawTask LongestTask, RawTask ShortestTask, int UsersCount) GetProjectInfoById(int id)
        //{
        //    var project = projectsService.GetProject(id);
        //    var projectTasks = projectsService.GetTasksAsync().Result.Where(x => x.ProjectId == project.Id);
        //    var users = projectsService.GetUsersAsync().Result;

        //    return
        //        (
        //            Project: project,
        //            LongestTask: projectTasks.OrderByDescending(t => t.Description).FirstOrDefault(),
        //            ShortestTask: projectTasks.OrderBy(t => t.Name).FirstOrDefault(),
        //            UsersCount: (projectTasks.Count() < 3 || project.Description.Length > 25 ? 
        //                                        users.Where(x => x.TeamId == project.Team.Id).Count() : -1)
        //        );
        //}
        #endregion
    }
}
