﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LINQ.Model
{
    class Task
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime FinishedAt { get; set; }

        public int StateId { get; set; }
        public int ProjectId { get; set; }
        public int PerformerId { get; set; }

        public Task() { }
        
        public override string ToString()
        {
            return $"{Id} - {Name} - {Description}";
        }
    }
}
