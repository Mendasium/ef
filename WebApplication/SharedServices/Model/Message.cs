﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SharedServices.Model
{
    public class Message
    {
        public int Id { get; set; }
        public DateTime DateTimeCreated { get; set; }

        public string MessageBody { get; set; }

        public override string ToString()
        {
            return $"{Id} - {MessageBody} - {DateTimeCreated.TimeOfDay}";
        }
    }
}
