﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using TaskProject.DataAccess;
using TaskProject.Model;

namespace TaskProject.Repositories
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : Entity
    {
        TaskProjectDBContext _context;

        public Repository(TaskProjectDBContext context)
        {
            _context = context;
        }

        public void Create(TEntity entity)
        {
            if (entity.Id != 0)
                entity.Id = 0;
            _context.Set<TEntity>().Add(entity);
        }

        public void Delete(TEntity entity)
        {
            var e = _context.Set<TEntity>().FirstOrDefault(x => x.Id == entity.Id);
            if (e != null)
                _context.Remove(e);
        }

        public void Delete(int id)
        {
            var e = _context.Set<TEntity>().FirstOrDefault(x => x.Id == id);
            if (e != null)
                _context.Remove(e);
        }

        public IEnumerable<TEntity> Get(Func<TEntity, bool> filter = null)
        {
            var entities = _context.Set<TEntity>();
            if(filter != null)
                return entities.Where(filter);
            return entities;
        }

        public void Update(TEntity entity)
        {
            TEntity e = _context.Set<TEntity>().FirstOrDefault(x => x.Id == entity.Id);
            if (e != null)
            {
                e.Update(entity);
                _context.Update(e);
            }
        }

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}
