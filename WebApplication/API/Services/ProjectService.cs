﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using TaskProject.Model;
using TaskProject.Model.DTOModel;
using TaskProject.Repositories;
using TaskProject.Services.ServicesAbstraction;

namespace TaskProject.Services
{
    public class ProjectService : IProjectService
    {
        IRepository<Project> _projects;
        IRepository<User> _users;
        IRepository<Team> _teams;
        IRepository<Task> _tasks;
        IRepository<State> _states;
        IQueueService _queueService;
        IMapper _mapper;

        public ProjectService(IRepository<Project> projects, IRepository<User> users, IRepository<Team> teams, IRepository<Task> tasks, IRepository<State> states, IQueueService queueService, IMapper mapper)
        {
            _states = states;
            _mapper = mapper;
            _tasks = tasks;
            _users = users;
            _projects = projects;
            _teams = teams;
            _queueService = queueService;
        }

        public void Create(Project entity)
        {
            if (entity.AuthorId == 0)
                throw new Exception("The project can not has no author");
            var team = _users.Get(new Func<User, bool>(x => x.Id == entity.AuthorId)).FirstOrDefault();
            entity.Author = team ?? throw new Exception("Such Author doesnt exist");

            if (entity.TeamId != 0)
            {
                entity.Team = _teams.Get(new Func<Team, bool>(x => x.Id == entity.TeamId)).FirstOrDefault();
            }

            entity.CreatedAt = DateTime.Now;
            entity.AuthorId = entity.TeamId = 0;
            _projects.Create(entity);
            _projects.Save();
            _queueService.Send($"Project {entity} was created");
        }

        public void Delete(Project entity)
        {
            _projects.Delete(entity);
            _projects.Save();
            _queueService.Send($"Project {entity} was removed");
        }

        public void Delete(int id)
        {
            _projects.Delete(id);
            _projects.Save();
            _queueService.Send($"Project {id} was removed");
        }

        public IEnumerable<Project> GetEntities(Func<Project, bool> filter = null)
        {
            _queueService.Send($"Getting all Entities " + (filter == null ? "" : " with filter"));
            return _projects.Get(filter);
        }

        public void Update(Project entity)
        {
            var project = _projects.Get(x => x.Id == entity.Id).FirstOrDefault();
            if (project != null)
            {
                if (entity.TeamId != 0)
                {
                    var team = _teams.Get(new Func<Team, bool>(x => x.Id == entity.TeamId)).FirstOrDefault();
                    if (team != null)
                        entity.Team = team;
                }
                _projects.Update(entity);
                _projects.Save();
                _queueService.Send($"Project {entity} was updated");
            }
        }

        public IEnumerable<(DTOProject Project, int TaskCount)> GetProjectTasksCountByUserId(int id)
        {
            var tasks = _tasks.Get();
            var projects = _projects.Get();

            return tasks.Where(t => t.PerformerId == id)
                .Join(projects, x => x.ProjectId, y => y.Id, (x,y) => new Task()
                {
                    Id = x.Id,
                    CreatedAt = x.CreatedAt,
                    Description = x.Description, 
                    Project = y,
                    ProjectId = x.ProjectId,
                    FinishedAt = x.FinishedAt,
                    Name = x.Name,
                    PerformerId = x.PerformerId,
                    StateId = x.StateId
                })
                .GroupBy(x => x.Project)
                .Select(x => (Project: _mapper.Map<Project, DTOProject>(x.Key), TaskCount: x.Count()));
        }

        public (DTOUser User, DTOProject LastProject, int CanceledTasksCount, DTOTask LongestTask, int LastProjectUserTasks) GetUserInfoById(int id)
        {
            var states = _states.Get();
            var user = _users.Get(x => x.Id == id).FirstOrDefault();
            var lastProject = _projects.Get().OrderBy(x => x.CreatedAt).LastOrDefault();
            var tasks = _tasks.Get(t => t.PerformerId == id)
                .Select(t =>
                    {
                        t.State = states.FirstOrDefault(x => x.Id == t.StateId);
                        return t;
                    });
            return
                (
                    User: _mapper.Map<User, DTOUser>(user),
                    LastProject: _mapper.Map<Project, DTOProject>(lastProject),
                    CanceledTasksCount: tasks.Count(x => x.State.Value == "Canceled" || x.State.Value == "Started"),
                    LongestTask: _mapper.Map<Task, DTOTask>(tasks.OrderBy(x => x.FinishedAt - x.CreatedAt).FirstOrDefault()),
                    LastProjectUserTasks: tasks.Count(x => x.ProjectId == lastProject.Id)
                );
        }

        public (DTOProject Project, DTOTask LongestTask, DTOTask ShortestTask, int UsersCount) GetProjectInfoById(int id)
        {
            var project = _projects.Get(x => x.Id == id).FirstOrDefault();
            var projectTasks = _tasks.Get(x => x.ProjectId == id);
            var users = _users.Get();

            return
                (
                    Project: _mapper.Map<Project, DTOProject>(project),
                    LongestTask: _mapper.Map<Task, DTOTask>(projectTasks.OrderByDescending(t => t.Description).FirstOrDefault()),
                    ShortestTask: _mapper.Map<Task, DTOTask>(projectTasks.OrderBy(t => t.Name).FirstOrDefault()),
                    UsersCount: (projectTasks.Count() < 3 || project.Description.Length > 25 ?
                                                users.Where(x => x.TeamId == project.TeamId).Count() : -1)
                );
        }
    }
}
