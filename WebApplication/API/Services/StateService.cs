﻿using TaskProject.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskProject.Repositories;
using Microsoft.AspNetCore.SignalR;
using TaskProject.Services.ServicesAbstraction;

namespace TaskProject.Services
{
    public class StateService : IService<State>
    {
        IRepository<State> _states;
        IHubContext<AnswerHub> _hubContext;
        IQueueService _queueService;

        public StateService(IRepository<State> states, IHubContext<AnswerHub> hubContext, IQueueService queueService)
        {
            _queueService = queueService;
            _states = states;
            _hubContext = hubContext;
            _queueService = queueService;
        }

        public void Create(State entity)
        {
            _states.Create(entity);
            _states.Save();
            _queueService.Send($"State {entity.ToString()} was created");
        }

        public void Delete(State entity)
        {
            _states.Delete(entity);
            _states.Save();
            _queueService.Send($"State {entity.ToString()} was removed");
        }

        public void Delete(int id)
        {
            _states.Delete(id);
            _states.Save();
            _queueService.Send($"State {id} was removed");
        }

        public IEnumerable<State> GetEntities(Func<State, bool> filter = null)
        {
            _queueService.Send($"Getting all States " + (filter == null ? "" : " with filter"));
            return _states.Get(filter);
        }

        public void Update(State entity)
        {
            _states.Update(entity);
            _states.Save();
            _queueService.Send($"State {entity.ToString()} was updated");
        }
    }
}
