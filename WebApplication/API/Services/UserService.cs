﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskProject.Model;
using TaskProject.Model.DTOModel;
using TaskProject.Repositories;
using TaskProject.Services.ServicesAbstraction;

namespace TaskProject.Services
{
    public class UserService : IUserService
    {
        IRepository<User> _users;
        IRepository<Team> _teams;
        IMapper _mapper;
        private IQueueService _queueService;

        public UserService(IRepository<User> users, IRepository<Team> teams, IQueueService queueService, IMapper mapper)
        {
            _mapper = mapper;
            _users = users;
            _teams = teams;
            _queueService = queueService;
        }

        public void Create(User entity)
        {
            if (entity.TeamId != 0)
            {
                entity.Team = _teams.Get(new Func<Team, bool>(x => x.Id == entity.TeamId)).FirstOrDefault();
            }

            entity.RegisteredAt = DateTime.Now;
            entity.TeamId = entity.Team?.Id;
            _users.Create(entity);
            _users.Save();
            _queueService.Send($"User {entity.ToString()} was added");
        }

        public void Delete(User entity)
        {
            _users.Delete(entity);
            _users.Save();
            _queueService.Send($"User {entity.ToString()} was removed");
        }

        public void Delete(int id)
        {
            _users.Delete(id);
            _users.Save();
            _queueService.Send($"User {id} was removed");
        }

        public IEnumerable<User> GetEntities(Func<User, bool> filter = null)
        {
            _queueService.Send($"Getting all Users " + (filter == null ? "" : " with filter"));
            return _users.Get(filter);
        }

        public void Update(User entity)
        {
            var user = _users.Get(x => x.Id == entity.Id).FirstOrDefault();
            if (user != null)
            {
                if (entity.TeamId != 0)
                {
                    var team = _teams.Get(new Func<Team, bool>(x => x.Id == entity.TeamId)).FirstOrDefault();
                    if (team != null)
                        entity.Team = team;
                }
                _users.Update(entity);
                _users.Save();
                _queueService.Send($"User {entity.ToString()} was updated");
            }
        }

        public IEnumerable<(int id, string name, IEnumerable<DTOUser> userList)> GetSortedTeams()
        {//Команд в которых все учасники старше 12 лет нету
            var users = _users.Get();
            var teams = _teams.Get();

            return users
                .Where(x => x.TeamId != null)
                .GroupBy(u => u.TeamId)
                .Where(u => u.All(x => DateTime.Now.Year - x.Birthday.Year >= 12))
                .Select(t =>
                    (id: (int)t.Key,
                        name: teams.FirstOrDefault(x => x.Id == t.Key).Name,
                    userList: _mapper.Map<IEnumerable<User>, IEnumerable<DTOUser>>(t.OrderByDescending(x => x.RegisteredAt)))
                    );
        }
    }
}
