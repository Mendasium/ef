﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskProject.Model;
using TaskProject.Repositories;
using TaskProject.Services.ServicesAbstraction;

namespace TaskProject.Services
{
    public class TeamService : IService<Team>
    {
        IRepository<Team> _teams;
        IQueueService _queueService;

        public TeamService(IRepository<Team> states, IQueueService queueService)
        {
            _teams = states;
            _queueService = queueService;
        }

        public void Create(Team entity)
        {
            entity.CreatedAt = DateTime.Now;
            _teams.Create(entity);  
            _teams.Save();
            _queueService.Send($"Team {entity.ToString()} was created");
        }

        public void Delete(Team entity)
        {
            _teams.Delete(entity);
            _teams.Save();
            _queueService.Send($"Team {entity.ToString()} was removed");
        }

        public void Delete(int id)
        {
            _teams.Delete(id);
            _teams.Save();
            _queueService.Send($"Team {id} was removed");
        }

        public IEnumerable<Team> GetEntities(Func<Team, bool> filter = null)
        {
            _queueService.Send($"Getting all Teams " + (filter == null ? "" : " with filter"));
            return _teams.Get(filter);
        }

        public void Update(Team entity)
        {
            var state = _teams.Get(x => x.Id == entity.Id).FirstOrDefault();
            if(state != null)
            {
                _teams.Update(entity);
                _teams.Save();
                _queueService.Send($"Team {entity.ToString()} was updated");
            }
        }
    }
}
