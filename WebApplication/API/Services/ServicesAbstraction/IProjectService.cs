﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskProject.Model;
using TaskProject.Model.DTOModel;

namespace TaskProject.Services.ServicesAbstraction
{
    public interface IProjectService : IService<Project>
    {
        IEnumerable<(DTOProject Project, int TaskCount)> GetProjectTasksCountByUserId(int id);
        (DTOUser User, DTOProject LastProject, int CanceledTasksCount, DTOTask LongestTask, int LastProjectUserTasks) GetUserInfoById(int id);
        (DTOProject Project, DTOTask LongestTask, DTOTask ShortestTask, int UsersCount) GetProjectInfoById(int id);
    }
}
