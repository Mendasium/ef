﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskProject.Model;

namespace TaskProject.Services
{
    public interface IService<TEntity> where TEntity : Entity
    {
        IEnumerable<TEntity> GetEntities(Func<TEntity, bool> filter = null);

        void Create(TEntity entity);
        void Update(TEntity entity);
        void Delete(TEntity entity);
        void Delete(int id);
    }
}
