﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskProject.Model;
using TaskProject.Model.DTOModel;

namespace TaskProject.Services.ServicesAbstraction
{
    public interface IUserService : IService<User>
    {
        IEnumerable<(int id, string name, IEnumerable<DTOUser> userList)> GetSortedTeams();
    }
}
