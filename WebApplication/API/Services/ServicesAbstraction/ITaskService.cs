﻿using System;
using System.Collections.Generic;
using System.Linq;
using TaskProject.Model;
using TaskProject.Model.DTOModel;

namespace TaskProject.Services.ServicesAbstraction
{
    public interface ITaskService : IService<Task>
    {
        IEnumerable<Task> GetShortnameTasksByUserId(int id);
        IEnumerable<(int Id, string Name)> GetFinishedTasksByUserId(int id, int year = 2019);
        IEnumerable<(DTOUser user, IEnumerable<DTOTask> tasks)> GetSortedUsersWithTasks();
    }
}
