﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using TaskProject.Model;
using TaskProject.Model.DTOModel;
using TaskProject.Repositories;
using TaskProject.Services.ServicesAbstraction;

namespace TaskProject.Services
{
    public class TaskService : ITaskService
    {
        IRepository<Task> _tasks;
        IRepository<User> _users;
        IRepository<Project> _projects;
        IRepository<State> _states;
        IQueueService _queueService;
        IMapper _mapper;

        public TaskService(IRepository<Task> tasks, IRepository<User> users, IRepository<Project> projects, IRepository<State> states, IQueueService queueService, IMapper mapper)
        {
            _tasks = tasks;
            _users = users;
            _projects = projects;
            _states = states;
            _mapper = mapper;
            _queueService = queueService;
        }

        public void Create(Task entity)
        {
            if (entity.ProjectId != 0)
            {
                var project = _projects.Get(new Func<Project, bool>(x => x.Id == entity.ProjectId)).FirstOrDefault();
                entity.Project = project ?? throw new Exception("Such project doesnt exist");
            }
            else
                throw new Exception("Project must be added");

            if (entity.PerformerId != 0)
            {
                var performer = _users.Get(new Func<User, bool>(x => x.Id == entity.PerformerId)).FirstOrDefault();
                entity.Performer = performer ?? throw new Exception("Such performer doesnt exist");
            }
            else
                throw new Exception("Performer must be added");
            
            if(entity.StateId != 0)
            {
                var state = _states.Get(new Func<State, bool>(x => x.Id == entity.StateId)).FirstOrDefault();
                entity.State = state ?? throw new Exception("Such state doesnt exist");
            }
            else
                throw new Exception("State must be added");

            entity.CreatedAt = DateTime.Now;
            _tasks.Create(entity);
            _tasks.Save();
            _queueService.Send($"Task {entity.ToString()} was created");
        }
        

        public void Delete(Task entity)
        {
            _tasks.Delete(entity);
            _tasks.Save();
            _queueService.Send($"Task {entity.ToString()} was removed");
        }

        public void Delete(int id)
        {
            _tasks.Delete(id);
            _tasks.Save();
            _queueService.Send($"Task {id} was removed");
        }

        public IEnumerable<Task> GetEntities(Func<Task, bool> filter = null)
        {
            _queueService.Send($"Getting all Tasks " + (filter == null ? "" : " with filter"));
            return _tasks.Get(filter);
        }

        public void Update(Task entity)
        {
            var task = _tasks.Get(x => x.Id == entity.Id).FirstOrDefault();
            if (task != null)
            {
                if (entity.StateId != 0)
                {
                    var state = _states.Get(x => x.Id == entity.StateId).FirstOrDefault();
                    if (state != null)
                        entity.State = state;
                }

                _tasks.Update(entity);
                _tasks.Save();
                _queueService.Send($"Task {entity.ToString()} was updated");
            }
        }

        public IEnumerable<Task> GetShortnameTasksByUserId(int id)
        {
            var tasks = _tasks.Get();
            var users = _users.Get();

            return tasks
                .Join(users, x => x.PerformerId, y => y.Id, (x,y) => new Task()
                {
                    Id = x.Id,
                    CreatedAt = x.CreatedAt,
                    Description = x.Description,
                    ProjectId = x.ProjectId,
                    FinishedAt = x.FinishedAt,
                    Name = x.Name,
                    PerformerId = x.PerformerId,
                    Performer = y,
                    StateId = x.StateId
                })
                .Where(t => t.Name.Length < 45 && t.Performer.Id == id);
        }

        public IEnumerable<(int Id, string Name)> GetFinishedTasksByUserId(int id, int year = 2019)
        {
            var tasks = _tasks.Get();
            var states = _states.Get();

            return tasks
                .Join(states, x => x.StateId, y => y.Id, (x,y) => new Task()
                {
                    PerformerId = x.PerformerId,
                    Id = x.Id,
                    Name = x.Name,
                    FinishedAt = x.FinishedAt,
                    State = y
                })
                .Where(t => t.PerformerId == id && t.FinishedAt.Year == year && t.State.Value.Equals("finished"))
                .Select(x => (Id: x.Id, Name: x.Name));
        }

        public IEnumerable<(DTOUser user, IEnumerable<DTOTask> tasks)> GetSortedUsersWithTasks()
        {
            var tasks = _tasks.Get();
            var users = _users.Get();

            return tasks.GroupBy(x => x.PerformerId)
                .Select(x => (
                        Performer: _mapper.Map<User, DTOUser>(users.FirstOrDefault(y => y.Id == x.Key)),
                        Tasks: _mapper.Map<IEnumerable<Task>, IEnumerable<DTOTask>>(x.OrderByDescending(y => y.Name.Length).ToList())))
                .OrderBy(x => x.Performer.FirstName);
        }
    }
}
