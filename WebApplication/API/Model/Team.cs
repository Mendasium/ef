﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskProject.Model
{
    public class Team : Entity
    {
        [Required]
        [MaxLength(20)]
        public string Name { get; set; }
        [Required]
        public DateTime CreatedAt { get; set; }

        public override string ToString()
        {
            return $"{Id} - {Name}";
        }

        public override void Update(Entity entity)
        {
            if (entity is Team team)
            {
                if (!string.IsNullOrEmpty(team.Name) && team.Name.Length > 3 && team.Name != Name)
                    this.Name = team.Name;
            }
            else
            {
                throw new FormatException("You need to use team entity here");
            }
        }
    }
}
