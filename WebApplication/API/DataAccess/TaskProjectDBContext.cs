﻿using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.ValueGeneration;
using System;
using System.Collections.Generic;
using System.Linq;
using TaskProject.Model;

namespace TaskProject.DataAccess
{
    public class TaskProjectDBContext : DbContext
    {
        public TaskProjectDBContext(DbContextOptions<TaskProjectDBContext> options) 
            : base(options)
        { }

        public DbSet<Task> Tasks { get; set; }
        public DbSet<State> States { get; set; }
        public DbSet<Project> Projects { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Team> Teams { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Task>()
                .Property(x => x.Id)
                .IsRequired()
                .ValueGeneratedOnAdd();
            modelBuilder.Entity<Task>()
                .Property(x => x.Name)
                .IsRequired()
                .HasMaxLength(20)
                .HasColumnName("TaskName");
            modelBuilder.Entity<Task>()
                .Property(x => x.StateId)
                .IsRequired()
                .HasColumnName("State"); 
            modelBuilder.Entity<Task>()
                .Property(x => x.PerformerId)
                .IsRequired()
                .HasColumnName("Performer");
            modelBuilder.Entity<Task>()
                .Property(x => x.ProjectId)
                .IsRequired()
                .HasColumnName("Project");
            modelBuilder.Entity<Task>()
                .Property(x => x.CreatedAt)
                .IsRequired();

            modelBuilder.Entity<Team>()
                .Property(x => x.CreatedAt)
                .IsRequired();

            modelBuilder.Entity<User>()
                .Property(x => x.RegisteredAt)
                .IsRequired();

            modelBuilder.Entity<Project>()
                .Property(x => x.CreatedAt)
                .IsRequired();

            AddSeeding(modelBuilder);

            base.OnModelCreating(modelBuilder);
        }

        protected void AddSeeding(ModelBuilder modelBuilder)
        {
            var dateTimeNow = new DateTime(2019, 7, 1, 0, 0, 0, 0, DateTimeKind.Local);

            var teams = new List<Team>
            {
                new Team(){ Id = 1, CreatedAt = dateTimeNow, Name = "FirstTeam" },
                new Team(){ Id = 2, CreatedAt = dateTimeNow, Name = "SecondTeam" },
                new Team(){ Id = 3, CreatedAt = dateTimeNow, Name = "ThirdTeam" }
            };

            var states = new List<State>
            {
                new State(){ Id = 1, Value = "Created" },
                new State(){ Id = 2, Value = "In progress" },
                new State(){ Id = 3, Value = "Done" }
            };

            var users = new List<User>
            {
                new User(){ Id = 1, FirstName = "Ivan", LastName = "Ivanenko", RegisteredAt = dateTimeNow, TeamId = 1, Birthday = dateTimeNow.AddYears(-20) },
                new User(){ Id = 2, FirstName = "Sergii", LastName = "Sergienko", RegisteredAt = dateTimeNow, TeamId = 2, Birthday = dateTimeNow.AddYears(-15) },
                new User(){ Id = 3, FirstName = "Oleksii", LastName = "Olekseenko", RegisteredAt = dateTimeNow, Birthday = dateTimeNow.AddYears(-30) }
            };

            var projects = new List<Project>
            {
                new Project(){ Id = 1, Name = "Project1", AuthorId = 1, CreatedAt = dateTimeNow, TeamId = 2 },
                new Project(){ Id = 2, Name = "Project3", AuthorId = 1, CreatedAt = dateTimeNow, TeamId = 2 },
                new Project(){ Id = 3, Name = "Project2", AuthorId = 3, CreatedAt = dateTimeNow, TeamId = 1 }
            };

            var tasks = new List<Task>
            {
                new Task(){ Id = 1, Name = "Task1", ProjectId = 1, CreatedAt = dateTimeNow, PerformerId = 2, StateId = 3, FinishedAt = dateTimeNow.AddDays(10) },
                new Task(){ Id = 2, Name = "Task2", ProjectId = 1, CreatedAt = dateTimeNow, PerformerId = 1, StateId = 2, FinishedAt = dateTimeNow.AddDays(20) },
                new Task(){ Id = 3, Name = "Task3", ProjectId = 3, CreatedAt = dateTimeNow, PerformerId = 3, StateId = 1, FinishedAt = dateTimeNow.AddDays(30) }
            };

            modelBuilder.Entity<State>().HasData(states);
            modelBuilder.Entity<Team>().HasData(teams);
            modelBuilder.Entity<User>().HasData(users);
            modelBuilder.Entity<Project>().HasData(projects);
            modelBuilder.Entity<Task>().HasData(tasks);
        }

        //private class DateTimeGenerator : ValueGenerator
        //{
        //    public override bool GeneratesTemporaryValues => false;

        //    protected override object NextValue(EntityEntry entry)
        //    {
        //        return DateTime.Now;
        //    }
        //}
    }

    
}
