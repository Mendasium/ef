﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TaskProject.Migrations
{
    public partial class TaskColumnsNamesUpdated : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Tasks_Users_PerformerId",
                table: "Tasks");

            migrationBuilder.DropForeignKey(
                name: "FK_Tasks_Projects_ProjectId",
                table: "Tasks");

            migrationBuilder.DropForeignKey(
                name: "FK_Tasks_States_StateId",
                table: "Tasks");

            migrationBuilder.RenameColumn(
                name: "StateId",
                table: "Tasks",
                newName: "State");

            migrationBuilder.RenameColumn(
                name: "ProjectId",
                table: "Tasks",
                newName: "Project");

            migrationBuilder.RenameColumn(
                name: "PerformerId",
                table: "Tasks",
                newName: "Performer");

            migrationBuilder.RenameIndex(
                name: "IX_Tasks_StateId",
                table: "Tasks",
                newName: "IX_Tasks_State");

            migrationBuilder.RenameIndex(
                name: "IX_Tasks_ProjectId",
                table: "Tasks",
                newName: "IX_Tasks_Project");

            migrationBuilder.RenameIndex(
                name: "IX_Tasks_PerformerId",
                table: "Tasks",
                newName: "IX_Tasks_Performer");

            migrationBuilder.AddForeignKey(
                name: "FK_Tasks_Users_Performer",
                table: "Tasks",
                column: "Performer",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Tasks_Projects_Project",
                table: "Tasks",
                column: "Project",
                principalTable: "Projects",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Tasks_States_State",
                table: "Tasks",
                column: "State",
                principalTable: "States",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Tasks_Users_Performer",
                table: "Tasks");

            migrationBuilder.DropForeignKey(
                name: "FK_Tasks_Projects_Project",
                table: "Tasks");

            migrationBuilder.DropForeignKey(
                name: "FK_Tasks_States_State",
                table: "Tasks");

            migrationBuilder.RenameColumn(
                name: "State",
                table: "Tasks",
                newName: "StateId");

            migrationBuilder.RenameColumn(
                name: "Project",
                table: "Tasks",
                newName: "ProjectId");

            migrationBuilder.RenameColumn(
                name: "Performer",
                table: "Tasks",
                newName: "PerformerId");

            migrationBuilder.RenameIndex(
                name: "IX_Tasks_State",
                table: "Tasks",
                newName: "IX_Tasks_StateId");

            migrationBuilder.RenameIndex(
                name: "IX_Tasks_Project",
                table: "Tasks",
                newName: "IX_Tasks_ProjectId");

            migrationBuilder.RenameIndex(
                name: "IX_Tasks_Performer",
                table: "Tasks",
                newName: "IX_Tasks_PerformerId");

            migrationBuilder.AddForeignKey(
                name: "FK_Tasks_Users_PerformerId",
                table: "Tasks",
                column: "PerformerId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Tasks_Projects_ProjectId",
                table: "Tasks",
                column: "ProjectId",
                principalTable: "Projects",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Tasks_States_StateId",
                table: "Tasks",
                column: "StateId",
                principalTable: "States",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
