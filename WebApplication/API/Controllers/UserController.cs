﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using TaskProject.Services;
using TaskProject.Model;
using AutoMapper;
using TaskProject.Model.DTOModel;
using System.Linq;
using TaskProject.Services.ServicesAbstraction;

namespace TaskProject.Controllers
{
    [Route("api/[controller]")]
    public class UserController : ControllerBase
    {
        IUserService _userService;
        IMapper _mapper;
        public UserController(IUserService userService, IMapper mapper)
        {
            _mapper = mapper;
            _userService = userService;
        }

        [HttpPost]
        public void Add([FromBody]DTOUser dTOUser)
        {
            var user = _mapper.Map<DTOUser, User>(dTOUser);
            _userService.Create(user);
        }

        [HttpPut]
        public void Update([FromBody]DTOUser dTOUser)
        {
            var user = _mapper.Map<DTOUser, User>(dTOUser);
            _userService.Update(user);
        }

        [HttpGet]
        public IEnumerable<DTOUser> GetAll()
        {
            return _mapper.Map<IEnumerable<User>, IEnumerable<DTOUser>>(_userService.GetEntities());
        }

        [HttpGet("{id}")]
        public DTOUser GetById(int id)
        {
            return _mapper.Map<User, DTOUser>(_userService.GetEntities(new Func<User, bool>(x => x.Id == id)).FirstOrDefault());
        }

        [HttpGet("getSortedTeams")]
        public IEnumerable<(int id, string name, IEnumerable<DTOUser> userList)> GetSortedTeams()
        {
            return _userService.GetSortedTeams();
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _userService.Delete(id);
        }

        [HttpDelete]
        public void Delete(DTOUser user)
        {
            _userService.Delete(_mapper.Map<DTOUser, User>(user));
        }
    }
}
