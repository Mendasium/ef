﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using TaskProject.Services;
using TaskProject.Model;
using AutoMapper;
using TaskProject.Model.DTOModel;
using System.Linq;

namespace TaskProject.Controllers
{
    [Route("api/[controller]")]
    public class TeamController : ControllerBase
    {
        IService<Team> _teamsService;
        IMapper _mapper;
        public TeamController(IService<Team> teamsService, IMapper mapper)
        {
            _mapper = mapper;
            _teamsService = teamsService;
        }

        [HttpPost]
        public void Add([FromBody]DTOTeam dTOTeam)
        {
            var team = _mapper.Map<DTOTeam, Team>(dTOTeam);
            _teamsService.Create(team);
        }

        [HttpPut]
        public void Update([FromBody]DTOTeam dTOTeam)
        {
            var team = _mapper.Map<DTOTeam, Team>(dTOTeam);
            _teamsService.Update(team);
        }

        [HttpGet]
        public IEnumerable<DTOTeam> GetAll()
        {
            return _mapper.Map<IEnumerable<Team>, IEnumerable<DTOTeam>>(_teamsService.GetEntities());
        }

        [HttpGet("{id}")]
        public DTOTeam GetById(int id)
        {
            return _mapper.Map<Team, DTOTeam>(_teamsService.GetEntities(new Func<Team, bool>(x => x.Id == id)).FirstOrDefault());
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _teamsService.Delete(id);
        }

        [HttpDelete]
        public void Delete(DTOTeam team)
        {   
            _teamsService.Delete(_mapper.Map<DTOTeam, Team>(team));
        }
    }
}
