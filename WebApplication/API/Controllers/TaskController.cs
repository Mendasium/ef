﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using TaskProject.Services;
using TaskProject.Model;
using AutoMapper;
using TaskProject.Model.DTOModel;
using System.Linq;
using TaskProject.Services.ServicesAbstraction;

namespace TaskProject.Controllers
{
    [Route("api/[controller]")]
    public class TaskController : ControllerBase
    {
        ITaskService _tasksService;
        IMapper _mapper;
        public TaskController(ITaskService tasksService, IMapper mapper)
        {
            _mapper = mapper;
            _tasksService = tasksService;
        }

        [HttpPost]
        public void Add([FromBody]DTOTask dTOTask)
        {
            var task = _mapper.Map<DTOTask, Task>(dTOTask);
            _tasksService.Create(task);
        }

        [HttpPut]
        public void Update([FromBody]DTOTask dTOTask)
        {
            var task = _mapper.Map<DTOTask, Task>(dTOTask);
            _tasksService.Update(task);
        }

        [HttpGet]
        public IEnumerable<DTOTask> GetAll()
        {
            return _mapper.Map<IEnumerable<Task>, IEnumerable<DTOTask>>(_tasksService.GetEntities());
        }

        [HttpGet("{id}")]
        public DTOTask GetById(int id)
        {
            return _mapper.Map<Task, DTOTask>(_tasksService.GetEntities(x => x.Id == id).FirstOrDefault());
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _tasksService.Delete(id);
        }

        [HttpDelete]
        public void Delete(DTOTask Task)
        {
            _tasksService.Delete(_mapper.Map<DTOTask, Task>(Task));
        }

        [HttpGet("sortedBusyUsers")]
        public IEnumerable<(DTOUser user, IEnumerable<DTOTask> tasks)> GetSortedUsersWithTasks()
        {
            return _tasksService.GetSortedUsersWithTasks();
        }

        [HttpGet("finishedTasks/{id}/{year}")]
        public IEnumerable<(int Id, string Name)> GetFinishedTasksByUserId(int id, int year = 2019)
        {
            return _tasksService.GetFinishedTasksByUserId(id, year);
        }

        [HttpGet("shortNameTasks/{id}")]
        public IEnumerable<Task> GetShortnameTasksByPerformerIdByUserId(int id)
        {
            return _tasksService.GetShortnameTasksByUserId(id);
        }
    }
}
