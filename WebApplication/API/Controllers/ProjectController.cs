﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using TaskProject.Services;
using TaskProject.Model;
using AutoMapper;
using TaskProject.Model.DTOModel;
using System.Linq;
using TaskProject.Services.ServicesAbstraction;

namespace TaskProject.Controllers
{
    [Route("api/[controller]")]
    public class ProjectController : ControllerBase
    {
        IProjectService _projectsService;
        IMapper _mapper;
        public ProjectController(IProjectService projectsService, IMapper mapper)
        {
            _mapper = mapper;
            _projectsService = projectsService;
        }

        [HttpPost]
        public void Add([FromBody]DTOProject dTOProject)
        {
            var project = _mapper.Map<DTOProject, Project>(dTOProject);
            _projectsService.Create(project);
        }

        [HttpPut]
        public void Update([FromBody]DTOProject dTOProject)
        {
            var project = _mapper.Map<DTOProject, Project>(dTOProject);
            _projectsService.Update(project);
        }

        [HttpGet]
        public IEnumerable<DTOProject> GetAll()
        {
            return _mapper.Map<IEnumerable<Project>, IEnumerable<DTOProject>>(_projectsService.GetEntities());
        }

        [HttpGet("{id}")]
        public DTOProject GetById(int id)
        {
            return _mapper.Map<Project, DTOProject>(_projectsService.GetEntities(new Func<Project, bool>(x => x.Id == id)).FirstOrDefault());
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _projectsService.Delete(id);
        }

        [HttpDelete]
        public void Delete(DTOProject Project)
        {
            _projectsService.Delete(_mapper.Map<DTOProject, Project>(Project));
        }

        [HttpGet("projectTasks/{id}")]
        public IEnumerable<(DTOProject Project, int TaskCount)> GetProjectTasksCountByUserId(int id)
        {
            return _projectsService.GetProjectTasksCountByUserId(id);
        }

        [HttpGet("userInfo/{id}")]
        public (DTOUser User, DTOProject LastProject, int CanceledTasksCount, DTOTask LongestTask, int LastProjectUserTasks) GetUserInfoById(int id)
        {
            return _projectsService.GetUserInfoById(id);
        }

        [HttpGet("projectInfo/{id}")]
        public (DTOProject Project, DTOTask LongestTask, DTOTask ShortestTask, int UsersCount) GetProjectInfoById(int id)
        {
            return _projectsService.GetProjectInfoById(id);
        }
    }
}
