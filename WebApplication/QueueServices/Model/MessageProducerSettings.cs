﻿using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Text;

namespace QueueService.Model
{
    public class MessageProducerSettings
    {
        public IModel Channel { get; set; }

        public PublicationAddress PublicationAddress { get; set; }
    }
}
