﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QueueServices.Abstractions
{
    public interface IMessageProducer
    {
        void Send(string message, string type = null);
        void SendTyped(string message, Type type);
    }
}
