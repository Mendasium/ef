﻿using System;
using System.Collections.Generic;
using System.Text;
using QueueService.Model;
using QueueServices.Abstractions;
using RabbitMQ.Client;

namespace QueueServices.Services
{
    public class MessageQueue : IMessageQueue
    {
        IConnection _connection;
        public IModel Channel { get; set; }

        public MessageQueue(IConnectionFactory connection)
        {
            _connection = connection.CreateConnection();
            Channel = _connection.CreateModel();
        }

        public MessageQueue(IConnectionFactory connectionFactory, MessageScopedSettings messageScopedSettings) : this(connectionFactory)
        {
            DeclareExchange(messageScopedSettings.ExchangeName, messageScopedSettings.ExchangeType);

            if (messageScopedSettings.QueueName != null)
                DeclareQueue(messageScopedSettings.ExchangeName, messageScopedSettings.RoutingKey, messageScopedSettings.QueueName);
        }

        public void DeclareExchange(string name, string type)
        {
            Channel.ExchangeDeclare(name, type == null ? "" : type); 
        }
        
        public void DeclareQueue(string exchangeName, string routingKey, string queueName)
        {
            Channel.QueueDeclare(queue: queueName,
                                 durable: true,
                                 exclusive: false,
                                 autoDelete: false,
                                 arguments: null);
            Channel.QueueBind(queueName, exchangeName, routingKey);
        }

        public void Dispose()
        {
            _connection?.Dispose();
            Channel?.Dispose();
        }
    }
}
