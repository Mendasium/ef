﻿using QueueService.Model;
using QueueServices.Abstractions;
using QueueServices.Services;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using SharedServices.Model;
using SharedServices.Writer.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Worker
{
    class QueueService
    {
        private readonly IMessageProducerScoped _messageProducerScope;
        private readonly IMessageConsumerScoped _messageConsumerScope;
        private readonly IWriterReader<Message> _writerReader;

        public QueueService(IMessageProducerScopedFactory messageProducerScope, IMessageConsumerScopedFactory messageConsumerScope, IWriterReader<Message> writerReader)
        {
            _writerReader = writerReader;

            _messageConsumerScope = messageConsumerScope.Open(new MessageScopedSettings()
            {
                ExchangeType = ExchangeType.Topic,
                ExchangeName = "ServerExchange",
                QueueName = "requestQueue",
                RoutingKey = "request"
            });

            _messageProducerScope = messageProducerScope.Open(new MessageScopedSettings()
            {
                ExchangeType = ExchangeType.Direct,
                ExchangeName = "ClientExchange",
                QueueName = "responsesQueue",
                RoutingKey = "response"
            });
            _messageConsumerScope.MessageConsumer.Connect();
            _messageConsumerScope.MessageConsumer.Received += GetValue;
        }

        public void SendMessage(string message)
        {
            _messageProducerScope.MessageProducer.Send(message);
        }

        private void GetValue(object sender, BasicDeliverEventArgs e)
        {
            try
            {
                Message m = new Message()
                {
                    DateTimeCreated = DateTime.Now,
                    MessageBody = Encoding.UTF8.GetString(e.Body)
                };
                _writerReader.Write(m);
                _messageConsumerScope.MessageConsumer.SetAcknowledge(e.DeliveryTag, true);
                Console.WriteLine(m);
                SendAnswer("Successfully received");
            }
            catch(Exception exc)
            {
                _messageConsumerScope.MessageConsumer.SetAcknowledge(e.DeliveryTag, false);
                Console.WriteLine($"Error {exc.Message}");
                SendAnswer($"While handling a message, smth goes wrong {exc.Message}");
            }
        }

        private void SendAnswer(string message)
        {
            _messageProducerScope.MessageProducer.Send(message);
        }
    }
}
